package com.juicy.alarmclock

import android.app.Activity
import android.app.TimePickerDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.View
import android.widget.Switch
import android.widget.TextView
import com.juicy.alarmclock.adapter.AlarmViewAdapter
import com.juicy.alarmclock.adapter.DayOfWeekViewAdapter
import com.juicy.alarmclock.model.Alarm
import kotlinx.android.synthetic.main.activity_alarm_settings.*
import java.util.*

class AlarmSettingsActivity : AppCompatActivity() {

    private var alarm = Alarm()

    private lateinit var recyclerView: RecyclerView
    private lateinit var dayOfWeekViewAdapter: DayOfWeekViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_alarm_settings)

        if (intent.hasExtra("alarm")) {
            alarm = Alarm.fromBundle(intent.getBundleExtra("alarm"))
            setTime(alarm.time)
            enabled.isChecked = alarm.enabled
        }

        recyclerView = findViewById(R.id.repeat)
        recyclerView.layoutManager = LinearLayoutManager(this)
        dayOfWeekViewAdapter = DayOfWeekViewAdapter(alarm.recurrence)
        recyclerView.adapter = dayOfWeekViewAdapter

        enabled.setOnCheckedChangeListener { buttonView, isChecked ->
            alarm.enabled = isChecked
        }

        time.setOnClickListener {
            val calendar = alarm.time
            TimePickerDialog(this, { view, hourOfDay, minute ->
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                calendar.set(Calendar.MINUTE, minute)
                setTime(calendar)
            }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true).show()
        }
    }

    private fun setTime(calendar : Calendar) {
        time.setText(DateUtils.formatDateTime(this, calendar.timeInMillis, DateUtils.FORMAT_SHOW_TIME))
    }

    fun saveButtonClick(v: View) {
        val result = Intent()
        alarm.recurrence = dayOfWeekViewAdapter.dow
        result.putExtra("alarm", alarm.toBundle())
        setResult(Activity.RESULT_OK, result)
        finish()
    }

    fun cancelButtonClick(v : View) {
        setResult(Activity.RESULT_CANCELED, null)
        finish()
    }
}
