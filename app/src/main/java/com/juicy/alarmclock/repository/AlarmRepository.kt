package com.juicy.alarmclock.repository

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.juicy.alarmclock.model.Alarm

class AlarmRepository(context: Context) :
    SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_TABLE = "CREATE TABLE $TABLE_NAME (" +
                "$ID Integer PRIMARY KEY," +
                "$TIME Integer," +
                "$ENABLED Numeric," +
                "$RECURRENCE Integer" +
                ")"
        db?.execSQL(CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    fun save(alarm: Alarm) : Alarm {
        val db = this.writableDatabase
        val values = ContentValues().apply {
            put(TIME, alarm.time.timeInMillis)
            put(ENABLED, if (alarm.enabled) 1 else 0)
            put(RECURRENCE, alarm.recurrence)
        }
        if (alarm.id == null) {
            val res = db.insert(TABLE_NAME, null, values)
            alarm.id = res.toInt()
        } else {
            db.update(TABLE_NAME, values, "$ID = ?", arrayOf(alarm.id.toString()))
        }
        db.close()
        return alarm
    }

    fun remove(alarm: Alarm) {
        if (alarm.id != null) {
            val db = this.writableDatabase
            db.delete(TABLE_NAME, "$ID = ?", arrayOf(alarm.id.toString()))
        }
    }

    fun getAll(): List<Alarm> {
        val alarmList = mutableListOf<Alarm>()
        val db = this.readableDatabase
        val query = "SELECT * FROM $TABLE_NAME"
        val cursor = db.rawQuery(query, null)
        with (cursor) {
            while (moveToNext()) {
                alarmList.add(Alarm().apply {
                    id = cursor.getInt(cursor.getColumnIndex(ID))
                    time.timeInMillis = cursor.getLong(cursor.getColumnIndex(TIME))
                    enabled = cursor.getInt(cursor.getColumnIndex(ENABLED)) > 0
                    recurrence = cursor.getInt(cursor.getColumnIndex(RECURRENCE))
                })
            }
        }
        cursor.close()
        db.close()
        return alarmList
    }

    companion object {
        private val DB_NAME = "AlarmClockDB"
        private val DB_VERSION = 1
        private val TABLE_NAME = "alarms"
        private val ID = "id"
        private val TIME = "time"
        private val ENABLED = "enabled"
        private val RECURRENCE = "recurrence"
    }
}