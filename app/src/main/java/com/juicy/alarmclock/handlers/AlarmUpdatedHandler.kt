package com.juicy.alarmclock.handlers

import com.juicy.alarmclock.model.Alarm

interface AlarmUpdatedHandler {

    fun onChange(alarm : Alarm)
}