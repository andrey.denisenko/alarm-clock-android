package com.juicy.alarmclock.handlers

import com.juicy.alarmclock.model.Alarm

interface AlarmDeletionHandler {
    fun onDelete(alarm : Alarm)
}