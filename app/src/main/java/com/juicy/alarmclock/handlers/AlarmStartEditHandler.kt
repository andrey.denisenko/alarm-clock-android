package com.juicy.alarmclock.handlers

import com.juicy.alarmclock.model.Alarm

interface AlarmStartEditHandler {

    fun onStartEdit(alarm : Alarm)
}