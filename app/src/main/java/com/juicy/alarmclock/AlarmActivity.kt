package com.juicy.alarmclock

import android.media.RingtoneManager
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.format.DateUtils
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import com.juicy.alarmclock.model.Alarm
import java.util.*


class AlarmActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alarm)
        val alarm = Alarm.fromBundle(intent.getBundleExtra("alarm"))
        val alarmTimeTextView: TextView = findViewById(R.id.alarmTimeTextView)
        alarmTimeTextView.text = DateUtils.formatDateTime(this, alarm.time.timeInMillis, DateUtils.FORMAT_SHOW_TIME)
        val disableAlarmButton: Button = findViewById(R.id.disableAlarmButton)
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)
        var alarmUri: Uri? = RingtoneManager.getActualDefaultRingtoneUri(this, RingtoneManager.TYPE_ALARM)
        if (alarmUri == null) {
            alarmUri = RingtoneManager.getActualDefaultRingtoneUri(this, RingtoneManager.TYPE_NOTIFICATION)
        }
        val ringtone = RingtoneManager.getRingtone(this, alarmUri)
        ringtone.play()
        disableAlarmButton.setOnClickListener {
            ringtone.stop()
            this.finish()
        }

    }
}
