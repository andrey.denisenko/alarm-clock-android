package com.juicy.alarmclock.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.juicy.alarmclock.R
import com.juicy.alarmclock.model.DaysOfWeek

class DayOfWeekViewAdapter(
    var dow : Int
) : RecyclerView.Adapter<DayOfWeekViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.dayofweek_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = 7

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if ((dow shr (6 - position)) and 1 == 1) {
            holder.selected.visibility = View.VISIBLE
        } else {
            holder.selected.visibility = View.INVISIBLE
        }
        holder.text.text = DaysOfWeek.NAMES[position]
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val text : TextView = view.findViewById(R.id.dowName)
        val selected : ImageView = view.findViewById(R.id.dowSelected)
        val itemView : View = view.apply {
            setOnClickListener {
                dow = dow xor (1 shl (6 - adapterPosition))
                notifyItemChanged(adapterPosition)
            }
        }
    }

}