package com.juicy.alarmclock.adapter

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import android.widget.TextView
import com.juicy.alarmclock.R
import com.juicy.alarmclock.handlers.AlarmDeletionHandler
import com.juicy.alarmclock.handlers.AlarmStartEditHandler
import com.juicy.alarmclock.handlers.AlarmUpdatedHandler
import com.juicy.alarmclock.helpers.ItemTouchHelperAdapter
import com.juicy.alarmclock.model.Alarm
import com.juicy.alarmclock.model.DaysOfWeek

class AlarmViewAdapter(
    private val alarmList: List<Alarm>,
    private val alarmUpdatedHandler: AlarmUpdatedHandler,
    private val alarmStartEditHandler: AlarmStartEditHandler,
    private val alarmDeletionHandler: AlarmDeletionHandler,
    private val context: Context
) : RecyclerView.Adapter<AlarmViewAdapter.ViewHolder>(), ItemTouchHelperAdapter {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.alarm_card_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(alarmList[position]) {
            holder.time.text = DateUtils.formatDateTime(context, time.timeInMillis, DateUtils.FORMAT_SHOW_TIME)
            holder.enabled.isChecked = enabled
            if (recurrence == 0b1111111) {
                holder.recurrence.text = context.getString(R.string.everyday)
            } else if (recurrence == 0) {
                holder.recurrence.text = context.getString(R.string.no_recurrence)
            } else {
                val selectedDays = 0.rangeTo(6)
                    .filter { (recurrence shr (6 - it)) and 1 == 1 }
                    .joinToString { DaysOfWeek.NAMES[it] }
                holder.recurrence.text = context.getString(R.string.repeats_every, selectedDays)
            }
        }

    }

    override fun getItemCount() = alarmList.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val card: CardView = view.findViewById<CardView>(R.id.idCardView).apply {
            setOnClickListener {
                with(alarmList[adapterPosition]) {
                    alarmStartEditHandler.onStartEdit(this)
                }
            }
        }
        val time: TextView = view.findViewById(R.id.idAlarmTime)
        val recurrence: TextView = view.findViewById(R.id.idAlarmRecurrence)
        val enabled: Switch = view.findViewById<Switch>(R.id.idAlarmEnabled).apply {
            setOnCheckedChangeListener { buttonView, isChecked ->
                with(alarmList[adapterPosition]) {
                    enabled = isChecked
                    alarmUpdatedHandler.onChange(this)
                }
            }
        }
    }


    override fun onItemDismiss(position: Int) {
        alarmDeletionHandler.onDelete(alarmList[position])
        this.notifyDataSetChanged()
    }

}