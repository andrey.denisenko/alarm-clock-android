package com.juicy.alarmclock.helpers

interface ItemTouchHelperAdapter {
    fun onItemDismiss(position: Int)
}