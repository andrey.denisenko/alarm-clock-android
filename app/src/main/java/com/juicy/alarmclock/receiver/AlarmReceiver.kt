package com.juicy.alarmclock.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import android.media.RingtoneManager
import android.media.Ringtone
import android.net.Uri
import android.support.v4.app.ActivityCompat.startActivity
import android.support.v4.app.ActivityCompat.startActivityForResult
import android.support.v4.content.ContextCompat
import com.juicy.alarmclock.AlarmActivity
import com.juicy.alarmclock.model.Alarm
import com.juicy.alarmclock.repository.AlarmRepository


class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        Log.i("AlarmReceiver", "alarm received")
        if (context == null) {
            Log.w("AlarmReceiver", "context is null")
            return
        }

        if (intent == null) {
            Log.w("AlarmReceiver", "intent is null")
            return
        }

        val alarm = Alarm.fromBundle(intent.getBundleExtra("alarm"))
        val repository = AlarmRepository(context)
        if (alarm.recurrence == 0) {
            alarm.enabled = false
            repository.save(alarm)
        }

        val alarmActivityIntent = Intent(context, AlarmActivity::class.java)
        alarmActivityIntent.flags = (Intent.FLAG_ACTIVITY_NEW_TASK
                    or Intent.FLAG_ACTIVITY_CLEAR_TOP
                    or Intent.FLAG_ACTIVITY_SINGLE_TOP);
        alarmActivityIntent.putExtra("alarm", intent.getBundleExtra("alarm"))
        ContextCompat.startActivity(context, alarmActivityIntent, null)
    }
}