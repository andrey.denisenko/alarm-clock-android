package com.juicy.alarmclock

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.juicy.alarmclock.adapter.AlarmViewAdapter
import com.juicy.alarmclock.handlers.AlarmDeletionHandler
import com.juicy.alarmclock.handlers.AlarmStartEditHandler
import com.juicy.alarmclock.handlers.AlarmUpdatedHandler
import com.juicy.alarmclock.helpers.SimpleItemTouchHelperCallback
import com.juicy.alarmclock.model.Alarm
import com.juicy.alarmclock.receiver.AlarmReceiver
import com.juicy.alarmclock.repository.AlarmRepository
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var alarmRepository : AlarmRepository
    private lateinit var recyclerView : RecyclerView
    private lateinit var alarmViewAdapter : AlarmViewAdapter
    private lateinit var alarmManager: AlarmManager
    private var alarmList = mutableListOf<Alarm>()

    companion object {
        const val CREATE_ALARM_REQUEST_CODE = 1
        const val EDIT_ALARM_REQUEST_CODE = 2
        const val ID_OFFSET = 1_000_000 // because recurrent alarms can't be identified properly
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager

        alarmRepository = AlarmRepository(this)

        val alarms = alarmRepository.getAll()
        alarmList.addAll(0, alarms)

        recyclerView = findViewById(R.id.idAlarmRecyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)

        val alarmUpdatedHandler = object : AlarmUpdatedHandler {
            override fun onChange(alarm: Alarm) {
                alarmRepository.save(alarm)
                if (alarm.enabled)
                    setupAlarm(alarm)
                else
                    disableAlarm(alarm)
            }
        }

        val alarmStartEditHandler = object : AlarmStartEditHandler {
            override fun onStartEdit(alarm: Alarm) {
                val intent = Intent(this@MainActivity, AlarmSettingsActivity::class.java)
                intent.putExtra("alarm", alarm.toBundle())
                startActivityForResult(intent, EDIT_ALARM_REQUEST_CODE)
            }
        }

        val alarmDeletionHandler = object : AlarmDeletionHandler {
            override fun onDelete(alarm: Alarm) {
                alarmList.remove(alarm)
                alarmRepository.remove(alarm)
                disableAlarm(alarm)
            }

        }

        alarmViewAdapter = AlarmViewAdapter(alarmList, alarmUpdatedHandler, alarmStartEditHandler, alarmDeletionHandler,this)
        recyclerView.adapter = alarmViewAdapter

        val cb = SimpleItemTouchHelperCallback(alarmViewAdapter)
        val touchHelper = ItemTouchHelper(cb)
        touchHelper.attachToRecyclerView(recyclerView)

        fab.setOnClickListener { view ->
            val intent = Intent(this, AlarmSettingsActivity::class.java)
            startActivityForResult(intent, CREATE_ALARM_REQUEST_CODE)
        }
    }

    override fun onResume() {
        super.onResume()
        Log.i("MainActivity", "onResume")
        alarmList.clear()
        alarmList.addAll(0, alarmRepository.getAll())
        alarmViewAdapter.notifyDataSetChanged()
    }

    override fun onPause() {
        super.onPause()
        Log.i("MainActivity", "onPause")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            // save
            val bundle = data?.getBundleExtra("alarm")
            if (bundle != null) {
                val alarm = Alarm.fromBundle(bundle)
                alarmRepository.save(alarm)
                if (requestCode == CREATE_ALARM_REQUEST_CODE) {
                    alarmList.add(alarm)
                    alarmViewAdapter.notifyDataSetChanged()
                } else {
                    val chanded = alarmList.find { it.id == alarm.id }
                    val changedIndex = alarmList.indexOf(chanded)
                    alarmList.removeAt(changedIndex)
                    alarmList.add(changedIndex, alarm)
                    alarmViewAdapter.notifyItemChanged(changedIndex)
                }
                if (alarm.enabled)
                    setupAlarm(alarm)
                else
                    disableAlarm(alarm)
            }
        }
    }

    private fun setupAlarm(alarm: Alarm) {
        disableAlarm(alarm)
        if (alarm.recurrence == 0) {
            val alarmIntent = Intent(this, AlarmReceiver::class.java).let { intent ->
                intent.putExtra("alarm", alarm.toBundle())
                PendingIntent.getBroadcast(this, alarm.id!!, intent, 0)
            }
            val alarmTime: Calendar = Calendar.getInstance().apply {
                this.set(Calendar.HOUR_OF_DAY, alarm.time.get(Calendar.HOUR_OF_DAY))
                this.set(Calendar.MINUTE, alarm.time.get(Calendar.MINUTE))
                this.set(Calendar.SECOND, 0)
            }
            if (alarmTime.before(Calendar.getInstance())) {
                alarmTime.add(Calendar.DAY_OF_YEAR, 1)
            }
            alarmManager.set(
                AlarmManager.RTC_WAKEUP,
                alarmTime.timeInMillis,
                alarmIntent
            )
            Log.i("MainActivity", "$alarmTime was setup")
        } else {
            0.rangeTo(6)
                .filter { (alarm.recurrence shr (6 - it)) and 1 == 1 }
                .forEach {
                    val alarmIntent = Intent(this, AlarmReceiver::class.java).let { intent ->
                        intent.putExtra("alarm", alarm.toBundle())
                        PendingIntent.getBroadcast(this, ID_OFFSET * (it + 1) + alarm.id!!, intent, 0)
                    }
                    val alarmTime: Calendar = Calendar.getInstance().apply {
                        this.set(Calendar.DAY_OF_WEEK, it + 1)
                        this.set(Calendar.HOUR_OF_DAY, alarm.time.get(Calendar.HOUR_OF_DAY))
                        this.set(Calendar.MINUTE, alarm.time.get(Calendar.MINUTE))
                        this.set(Calendar.SECOND, 0)
                    }
                    if (alarmTime.before(Calendar.getInstance())) {
                        alarmTime.add(Calendar.WEEK_OF_YEAR, 1)
                    }
                    alarmManager.setRepeating(
                        AlarmManager.RTC_WAKEUP,
                        alarmTime.timeInMillis,
                        AlarmManager.INTERVAL_DAY * 7,
                        alarmIntent
                    )
                    Log.i("MainActivity", "$alarmTime was setup")
                }
        }
    }

    private fun disableAlarm(alarm: Alarm) {
        if (alarm.recurrence == 0) {
            val alarmIntent = Intent(this, AlarmReceiver::class.java).let { intent ->
                intent.putExtra("alarm", alarm.toBundle())
                PendingIntent.getBroadcast(this, alarm.id!!, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            }
            alarmManager.cancel(alarmIntent)
        } else {
            0.rangeTo(6)
                .filter { (alarm.recurrence shr (6 - it)) and 1 == 1 }
                .forEach {
                    val alarmIntent = Intent(this, AlarmReceiver::class.java).let { intent ->
                        intent.putExtra("alarm", alarm.toBundle())
                        PendingIntent.getBroadcast(this, ID_OFFSET * (it + 1) + alarm.id!!, intent, PendingIntent.FLAG_UPDATE_CURRENT)
                    }
                    alarmManager.cancel(alarmIntent)
                }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
