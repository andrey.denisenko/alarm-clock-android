package com.juicy.alarmclock.model

object DaysOfWeek {

    val NAMES = arrayOf("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
}