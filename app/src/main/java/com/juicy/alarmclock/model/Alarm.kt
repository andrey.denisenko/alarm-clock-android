package com.juicy.alarmclock.model

import android.os.Bundle
import java.util.*

class Alarm(
    var id: Int? = null,
    var time: Calendar = Calendar.getInstance(),
    var enabled: Boolean = true,
    var recurrence: Int = 0
) {
    fun toBundle() = Bundle().apply {
        putLong(TIME, time.timeInMillis)
        putBoolean(ENABLED, enabled)
        putInt(RECURRENCE, recurrence)
        if (id != null) {
            putInt(ID, id!!)
        }
    }

    companion object {
        const val ID = "id"
        const val TIME = "time"
        const val ENABLED = "enabled"
        const val RECURRENCE = "recurrence"

        fun fromBundle(bundle: Bundle) = Alarm().apply {
            if (bundle.containsKey(ID)) {
                id = bundle.getInt(ID)
            }
            if (bundle.containsKey(TIME)) {
                time.timeInMillis = bundle.getLong(TIME)
            }
            if (bundle.containsKey(ENABLED)) {
                enabled = bundle.getBoolean(ENABLED)
            }
            if (bundle.containsKey(RECURRENCE)) {
                recurrence = bundle.getInt(RECURRENCE)
            }
        }

    }
}